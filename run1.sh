#!/bin/sh -ex

docker network create --attachable --opt com.docker.network.bridge.name=docker docker || echo $?
docker volume create etcd1
docker stop etcd1 || echo $?
docker rm etcd1 || echo $?
docker run \
    --detach \
    --env ETCD_ADVERTISE_CLIENT_URLS=http://etcd1.docker:2379 \
    --env ETCD_DATA_DIR=/var/lib/etcd \
    --env ETCD_INITIAL_ADVERTISE_PEER_URLS=http://etcd1.docker:2380 \
    --env ETCD_INITIAL_CLUSTER=etcd1=http://etcd1.docker:2380,etcd2=http://etcd2.docker:2380,etcd3=http://etcd3.docker:2380 \
    --env ETCD_LISTEN_CLIENT_URLS=http://0.0.0.0:2379 \
    --env ETCD_LISTEN_PEER_URLS=http://0.0.0.0:2380 \
    --env ETCD_NAME=etcd1 \
    --env GROUP_ID="$(id -g)" \
    --env LANG=ru_RU.UTF-8 \
    --env TZ=Asia/Yekaterinburg \
    --env USER_ID="$(id -u)" \
    --hostname etcd1.docker \
    --mount type=bind,source=/etc/certs,destination=/etc/certs,readonly \
    --mount type=volume,source=etcd1,destination=/var/lib/etcd \
    --name etcd1 \
    --network name=docker,alias=etcd.docker \
    --restart always \
    rekgrpth/etcd su-exec etcd etcd
