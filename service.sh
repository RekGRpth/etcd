#!/bin/sh -ex

#docker build --tag rekgrpth/etcd .
#docker push rekgrpth/etcd
docker pull rekgrpth/etcd
docker network create --attachable --driver overlay docker || echo $?
docker volume create etcd
docker service rm etcd || echo $?
docker service create \
    --env LANG=ru_RU.UTF-8 \
    --env TZ=Asia/Yekaterinburg \
    --hostname tasks.etcd \
    --name etcd \
    --network name=docker \
    rekgrpth/etcd
