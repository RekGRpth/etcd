#!/bin/sh -ex

#docker build --tag rekgrpth/etcd .
#docker push rekgrpth/etcd
#docker pull rekgrpth/etcd
docker network create --attachable --opt com.docker.network.bridge.name=docker docker || echo $?
docker volume create etcd
docker stop etcd || echo $?
docker rm etcd || echo $?
docker run \
    --detach \
    --env GROUP_ID="$(id -g)" \
    --env LANG=ru_RU.UTF-8 \
    --env TZ=Asia/Yekaterinburg \
    --env USER_ID="$(id -u)" \
    --hostname etcd \
    --mount type=bind,source=/etc/certs,destination=/etc/certs,readonly \
    --mount type=volume,source=etcd,destination=/var/lib/etcd \
    --name etcd \
    --network name=docker \
    --restart always \
   rekgrpth/etcd
