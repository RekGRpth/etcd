FROM alpine
RUN exec 2>&1 \
    && set -ex \
    && apk add --no-cache --repository http://dl-cdn.alpinelinux.org/alpine/edge/testing --virtual .edge-rundeps \
        etcd \
    && apk add --no-cache --virtual .etcd-rundeps \
        musl-locales \
        shadow \
        su-exec \
        tzdata \
    && rm -rf /usr/src /usr/share/doc /usr/share/man /usr/local/share/doc /usr/local/share/man \
    && echo done
ADD bin /usr/local/bin
CMD [ "etcd" ]
ENTRYPOINT [ "docker_entrypoint.sh" ]
ENV HOME=/var/lib/etcd
ENV GROUP=etcd \
    USER=etcd
VOLUME "${HOME}"
WORKDIR "${HOME}"
RUN exec 2>&1 \
    && set -ex \
    && chmod -R 0755 /usr/local/bin \
    && echo done
